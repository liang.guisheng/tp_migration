from keycloak import KeycloakAdmin
from concurrent.futures.thread import ThreadPoolExecutor

keycloak_connection_config = {
    'homolog': {
        'shipper': {
            'server_url': 'http://keycloak.homolog.truckpad.com.br/auth/',
            'username': 'keycloak',
            'password': 'keycloak',
            'user_realm_name': 'master',
            'realm_name': 'shipper',
            'verify':False
        }
    },
    'staging': {
        'shipper': {
            'server_url': 'http://keycloak.svc.staging.truckpad.io/auth/',
            'username': 'keycloak',
            'password': 'keycloak',
            'user_realm_name': 'master',
            'realm_name': 'shipper',
            'verify':False
        }
    },
    # need to configure to correct 
    'production': {
        'shipper': {
            'server_url': 'http://keycloak.homolog.truckpad.com.br/auth/',
            'username': 'keycloak',
            'password': 'keycloak',
            'user_realm_name': 'master',
            'realm_name': 'shipper',
            'verify':False
        }
    }
}


def get_keycloak_admin(environment: str, realm_name: str):
    return KeycloakAdmin(**keycloak_connection_config[environment][realm_name])


def clean_keycloak_groups(admin: KeycloakAdmin):

    with ThreadPoolExecutor(max_workers=10) as executor:
        block = []
        for group in admin.get_groups():
            block.append(group['id'])
            if len(block) >= 200:
                executor.map(admin.delete_group, block[:])
                block = []
        if block:
            executor.map(admin.delete_group, block[:])


def clean_keycloak_users(admin: KeycloakAdmin):
    with ThreadPoolExecutor(max_workers=10) as executor:
        block = []
        for user in admin.get_users():
            block.append(user['id'])
            if len(block) >= 200:
                executor.map(admin.delete_group, block[:])
                block = []
        if block:
            executor.map(admin.delete_group, block[:])




if __name__ == "__main__":

    # test for homolog shipper keycloak admin
    # homolog_shipper_admin=get_keycloak_admin('homolog', 'shipper')
    # print(homolog_shipper_admin.get_user('cfd9b77d-ce20-48d6-8f8a-3a9a6e426043'))

    # test for staging shipper keycloak admin
    # staging_shipper_admin=get_keycloak_admin('staging', 'shipper')
    # print(staging_shipper_admin.get_user('686039ae-4c81-400a-a081-ed999134ccbd'))


    # 




