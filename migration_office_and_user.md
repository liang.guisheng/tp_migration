## 1. Preparation

- *data source* configurations: MySQL database connection from the correct environment

- *data destination* configurations: Mongo database where to save Users and Offices

- *SNS topics* configurations: topics for users and offices, and subscription the correct sqs of bi to these topics

- *bi-listener* deployed in correct environment with postgres database

- *keycloak* deployed in correct environment 

- *keycloak* realm configurations: shipper, driver, integration and jwt_private keys

- *api-gateway* configuration: need to update the JWT_KEYS include the *issuer=public_key* pairs for *driver*, *shipper* and *integration*

- *keycloak* integration realm configuration: create a internal service user and set token expiration to a long period(1200 days), and then use this user to generate its corresponding jwt access_token. This token need to be configured in the CI/CD variable `INTERNAL_SERVICE_JWT_TOKEN` of *deals-api* with the correct environment 

- *keycloak* shipper realm's **api** client's roles should be synced to **users-api**, because when create user with some specified **role**, *keycloak* should know the role object 

- *keycloak* shipper realm's **api** client's uuid should also be synced to **users-api**, add user client role need to use it

- *keycloak* shipper realm's **api** clent's uuid and secret_key should be synced to **jwt-sessions**, because jwt_session need them to init an KeycloakOpenId instance to generate user's jwt access_token


## 2. Migration scripts

2.1 In **offices-api**, we run the *truckpad/migrate_mysql_offices.py*, this script will import all offices from MySQL to Mongo and send SNS to BI and save **customers** and **offices** in postgres database

2.2 In **users-api**, we run the *users/script/migrate_tms_usres.py*, this script will import all users from MySQL to Mongo and create corresponding user with the right roles in *Keycloak* 

2.3 In **offices-api**, we run the *truckpad/sync_customer_and_office_to_keycloak*, this script will do the following: 
    - prepare some adaptation for the duplicated name of customers and offices and create related index in MongoDB
    - create index for customer's name and keycloak_alias_name field

    - create keycloak groups based on all customers  
    - create full_text index for customer's name field
    
    - sync keycloak corresponding group id to customer in mongo

    - sync mongo offices to keycloak groups

2.4 In **users-api**, we run *users/script/associate_user_office_keycloak*, this script will increase user's offices total_users count and add current user to keycloak group









